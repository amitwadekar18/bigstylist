import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from "@angular/http";
@Injectable()
export class ContactUsServiceService {

  url: string = 'http://localhost:8080/BigStylist';

  constructor(private http: Http) { }
  public insertMessage(cu_name,cu_email,cu_mobileno,cu_message){ 
    debugger;
   
    var body = {
      cu_name:cu_name,
      cu_email:cu_email,
      cu_mobileno:cu_mobileno,
      cu_message:cu_message
    };
    var header = new Headers({'Content-Type':'application/json'});
    var requestOption = new RequestOptions({headers: header});
    return this.http.post(this.url + '/contactus',body,requestOption);
  }

}

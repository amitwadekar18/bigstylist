// import { Component, OnInit } from '@angular/core';
// import{ BeautyServices } from '../beautyservices.service';

// @Component({
//   selector: 'app-allservices',
//   templateUrl: './allservices.component.html',
//   styleUrls: ['./allservices.component.css']
// })
// export class AllservicesComponent implements OnInit {
//  allServices:any;
//  typelength:number;
//  types:any;
//  id:any;
//   constructor(private service:BeautyServices) {
//     debugger;
//     this.service.AllServices().
//     subscribe(Response=>{
//     var result=Response.json();
//     if(result.status== 'true'){
//       this.allServices=result.body;
//       var flag=[],output=[],l=result.body.length,i;
//       for(i=0;i<l;i++)
//       {
//         if(flag[result.body[i].s_type])continue;
//         flag[result.body[i].s_type]=true;
//         output.push(result.body[i].s_type)
//       }
//       this.types=output;
//       this.typelength=output.length;
//      // alert("all data fetched successfully");
//     }
//     else{
//       alert("something went wrong");
//     }
//     });
//    }

//   ngOnInit() {
//   }

// public addToCart(id)
// {
//   alert("service selected is"+id);
// }
// public deleteFromCart(id)
// {
//   alert("services deleted is"+id);
// }
// }


import { Component, OnInit, Inject} from '@angular/core';
//import { BeautyServiceService } from '../beautyservice.service';
import{ BeautyServices } from '../beautyservices.service';
import {LOCAL_STORAGE, WebStorageService} from 'angular-webstorage-service';


@Component({
  selector: 'app-allservices',
  templateUrl: './allservices.component.html',
  styleUrls: ['./allservices.component.css']
})
export class AllservicesComponent implements OnInit {

  allServices: any;
  typelength: number;
  types: any;
  id: any;
  key: string = 'cart';
  static list: any=[];

  constructor(private service: BeautyServices,@Inject(LOCAL_STORAGE) private storage: WebStorageService) {
      this.service.AllServices()
      .subscribe( response => {
        var result = response.json();
        console.log(result);

        if(result.status == "true"){

          this.allServices = result.body;
          var flags = [], output = [], l = result.body.length, i;
          for( i=0; i<l; i++) {
              if( flags[result.body[i].s_type]) continue;
              flags[result.body[i].s_type] = true;
              output.push(result.body[i].s_type);
          }
          this.types =output;
          this.typelength = output.length;
        }else{
          alert('Failed');
        }
      });
   }

  ngOnInit() {
  }

  public addToCart(id,data){
    AllservicesComponent.list[this.key]=this.storage.get(this.key);
    ///debugger;
    console.log("key "+ this.key+'value: '+id);
    AllservicesComponent.list.push({id: id,data: data});
    this.storage.set(this.key,AllservicesComponent.list);
  }

  public deleteFromCart(id){
    var list2: any=[];
    var i: number;
    var data: any=[];
    var flag:number;
    var flag2:number=1;
    data[this.key]=this.storage.get(this.key);
   // debugger;
    for(i=0;i<data[this.key].length;i++)
    {
      if(data[this.key][i].id==id)
      {
        flag2=0;
      
      }
    }
    flag=0;
  if(flag2==0)
    {
     for(i=0;i<data[this.key].length;i++)
     {
      //  if(data[this.key][i].id!=id){
      // list2.push({id:data[this.key][i].id,name:data[this.key][i].name});
      if(data[this.key][i].id==id)
      {
          if(flag==0)
          {
            flag=1;
            continue;
          }
      }
      list2.push(data[this.key][i]);
      }
      AllservicesComponent.list=list2;
     this.storage.set(this.key,list2);
  }
    }

  }
//}

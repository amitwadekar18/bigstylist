import { Injectable } from '@angular/core';
import { Http,Headers,RequestOptions} from '@angular/http';
@Injectable()
export class BeautyServices{
url : String = 'http://localhost:8080/BigStylist';
constructor(private http: Http) { }


public AllServices(){
  debugger;
  return this.http.get(this.url + '/allservices');
}

public addServices1(s_type,s_name,s_duration,s_price){
  debugger;
  var body = {
    s_type: s_type,
    s_name: s_name,
    s_duration: s_duration,
    s_price:s_price
  };
  var header = new Headers({'Content-Type':'application/json'});
  var requestOption = new RequestOptions({headers: header});
  return this.http.post(this.url + '/addservices',body,requestOption);
}

}

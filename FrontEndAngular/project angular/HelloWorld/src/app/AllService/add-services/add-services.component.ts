import { Component, OnInit } from '@angular/core';
import{ BeautyServices } from '../beautyservices.service';
@Component({
  selector: 'app-add-services',
  templateUrl: './add-services.component.html',
  styleUrls: ['./add-services.component.css']
})
export class AddServicesComponent implements OnInit {
  s_type:String;
  s_name:String;
  s_duration:number;
  s_price:number;
  constructor(private service:BeautyServices) { }

  ngOnInit() {
  }

  public clearServices()
  {
     this.s_type="";
     this.s_name="";
     this.s_duration=null;
     this.s_price=null;
  }


  public insertServices(){
    debugger;
    this.service.addServices1(this.s_type,this.s_name,this.s_duration,this.s_price)
    .subscribe( response =>{
      var result = response.json();
      console.log(result);
      if(result.status == 'true'){
        alert("Registered");
      }
      else{
        alert("something wrong");
      }
    });
  }

 
}

import { TestBed, inject } from '@angular/core/testing';

import { BeautyServices } from './beautyservices.service';

describe('BeautyServicesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BeautyServices]
    });
  });

  it('should be created', inject([BeautyServices], (service: BeautyServices) => {
    expect(service).toBeTruthy();
  }));
});

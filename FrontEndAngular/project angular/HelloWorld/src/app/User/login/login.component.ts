import { loginService } from './../loginService.component';
import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  c_email: string;
  c_password: string;
  constructor(
    private service: loginService,
    private router: Router)
    { }

    ngOnInit() {
    }

    public register(){
      this.router.navigate(['register']);
    }

  public loginCheck(){
  this.service.loginMethod(this.c_email,this.c_password)
    .subscribe( response =>{
      var result = response.json();
      console.log(result);
      if(result.status == 'true'){
        alert('WOW!!  ');

      }
      else{
        alert('Wrong Credential !!  ');
      }

    });
  }





}

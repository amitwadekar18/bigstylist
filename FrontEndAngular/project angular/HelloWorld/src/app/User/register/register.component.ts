import { Component, OnInit } from '@angular/core';
import { loginService } from '../loginService.component';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  c_email: string;
  c_name: string;
  c_password: string;
  c_mobileno:string;
  c_address:string;
  c_city:string;
  c_pincode:Number;
  constructor(private service: loginService) { }

  ngOnInit() {
  }

  public clear(){
    this.c_name="";
    this.c_email="";
    this.c_mobileno="";
    this.c_password="";
    this.c_city="";
    this.c_address="";
    this.c_pincode=null;
  }
  
  public registerUser(){
    debugger;
    this.service.insertUser(this.c_email,this.c_name,this.c_password,this.c_mobileno,this.c_address,this.c_city,this.c_pincode)
    .subscribe( response =>{
      var result = response.json();
      console.log(result);
      if(result.status == 'true'){
        alert("Registered");
      }
      else{
        alert("something wrong");
      }
    });


  }
}

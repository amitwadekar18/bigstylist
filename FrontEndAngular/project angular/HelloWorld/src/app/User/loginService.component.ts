import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from "@angular/http";

@Injectable()
export class loginService {
  url: string = 'http://localhost:8080/BigStylist';

  constructor(private http: Http) {
  }

  public loginMethod(c_email,c_password){
    debugger;
    console.log("lol");
    var body = {
      c_email: c_email,
      c_password: c_password
    };
    var header = new Headers({'Content-Type':'application/json'});
    var requestOption = new RequestOptions({headers: header});
    return this.http.post(this.url + '/login',body,requestOption);
  }

  public insertUser(c_email,c_name,c_password,c_mobileno,c_address,c_city,c_pincode){
    debugger;
    var body = {
      c_email: c_email,
      c_name: c_name,
      c_password: c_password,
      c_mobileno:c_mobileno,
      c_address:c_address,
      c_city:c_city,
      c_pincode:c_pincode
    };
    var header = new Headers({'Content-Type':'application/json'});
    var requestOption = new RequestOptions({headers: header});
    return this.http.post(this.url + '/register',body,requestOption);
  }
}

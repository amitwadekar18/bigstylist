import { Injectable } from '@angular/core';
import { Http,Headers,RequestOptions} from '@angular/http';
@Injectable()
export class CartServiceService {
  url : String = 'http://localhost:8080/BigStylist';
  finalServices: any;
  finalPrice: string;
  finalRate: string;
  finalBeauticianId: string;

  constructor(private http: Http) { }

  public AllBeautician(){
    debugger;
    return this.http.get(this.url + '/allbeautician');
  }

  public getPayment(){
    alert("payment done suceessfully");
  }
  public getFinalCart(finalServices,finalPrice,finalRate,finalBeauticinId){
    this.finalServices = finalServices;
    this.finalPrice = finalPrice;
    this.finalRate = finalRate;
    this.finalBeauticianId = finalBeauticinId;
  }

  public fetchFinalServices(){
    return this.finalServices;
  }

  public fetchFinalPrice(){
    return this.finalPrice;
  }

  public fetchFinalRate(){
    return this.finalRate;
  }

  public featchBeauticianId(){
    return this.finalBeauticianId;
  }

}

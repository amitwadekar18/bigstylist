import { Component, OnInit } from '@angular/core';
import { CartServiceService } from '../cart-service.service';
@Component({
  selector: 'app-final-transaction',
  templateUrl: './final-transaction.component.html',
  styleUrls: ['./final-transaction.component.css']
})
export class FinalTransactionComponent implements OnInit {
  finalServices: any;
  finalPrice: string;
  finalRate: string;
  finalBeauticianId: string;
  total: number;
  constructor(private service: CartServiceService) {
    this.finalServices = this.service.fetchFinalServices();
    this.finalPrice = this.service.fetchFinalPrice();
    this.finalRate = this.service.fetchFinalRate();
    this.finalBeauticianId = this.service.featchBeauticianId();
    if(this.finalRate!=null){
    this.total = parseInt(this.finalPrice) + parseInt(this.finalRate);
    }else{
      this.total = parseInt(this.finalPrice);
   }
  }

  ngOnInit() {
  }

}

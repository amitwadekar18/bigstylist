import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FinalTransactionComponent } from './final-transaction.component';

describe('FinalTransactionComponent', () => {
  let component: FinalTransactionComponent;
  let fixture: ComponentFixture<FinalTransactionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FinalTransactionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FinalTransactionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit,Inject} from '@angular/core';
import {LOCAL_STORAGE, WebStorageService} from 'angular-webstorage-service';
import { CartServiceService } from '../cart-service.service';
@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {
  key: string = 'cart';
  cartServices: any=[];
  output:any=[];
  beautician:any=[];
  selectedPrice:String;
  totalPrice:number;
  b_charges:number;
  selectedId:number;
  constructor(private Cartser: CartServiceService,@Inject(LOCAL_STORAGE) private storage: WebStorageService) {
    var i: number;
    var services: any;
   
    services = this.storage.get(this.key);
    for(i=0;i<services.length;i++){
    this.cartServices.push(services[i].data);
       }
  //  debugger;
  this.calculateQty();
  
  this.Cartser.AllBeautician().
  subscribe( response => {
  var result = response.json();
  console.log(result);

  if(result.status == "true")
  {
    this.beautician = result.body;
  debugger;
  }
  else
  {
    alert('Failed');
  }
   });
   this.total();
  }
  

  ngOnInit() {
  }
calculateQty()
{ 
  var set = new Set();
  var l:number=0;
  var k:number=0;
  var services: any;
  var cnt:number=0;
  var i:number;
  var j:number;
  var upperid:any;
  var cart: any=[];
  var cart2:any=[];
  var services = this.storage.get(this.key);
  for(i=0;i<services.length;i++)
  {
    var upperid=services[i].data.s_id;
    cnt=0;
    for(j=0;j<services.length;j++)
    {
      if(upperid==services[j].data.s_id)
      cnt++;
    }
  
        cart.push({id: services[i].data.s_id,data:services[i],qty: cnt});
      
       
  
  }
  this.calcUnique(cart);
  debugger;
 // alert(cart);
  //alert(set);
}


calcUnique(cart)
{
  var flag=[],l=cart.length,i;
  for(i=0;i<l;i++)
  {
    if(flag[cart[i].id])continue;
    flag[cart[i].id]=true;
    this.output.push({id:cart[i].id,data:cart[i],qty:cart[i].qty});
  }
}


public selectedB(id){
  var i;
  for(i=0;i<this.beautician.length;i++){
    if(this.beautician[i].b_id==id){
      this.selectedPrice = this.beautician[i].b_charges;
    }
  }
  debugger;
}
// selecteddB()
// {
//  alert("selected price is"+this.selectedPrice);
// }
total()
{  
  var abc:number;
  this.totalPrice=0;
  for(var i=0; i<this.output.length; i++)
  {
    debugger;
    this.totalPrice= this.totalPrice+(this.output[i].data.data.data.s_price * this.output[i].qty);
  }
  alert(this.totalPrice);  
}
public uploadCart(){
  this.Cartser.getFinalCart(this.output,this.totalPrice,this.selectedPrice,this.selectedId);
}
}
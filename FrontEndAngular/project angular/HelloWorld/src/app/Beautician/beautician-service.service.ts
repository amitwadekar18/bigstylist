import { Injectable } from '@angular/core';
import { Http,Headers,RequestOptions} from '@angular/http';

@Injectable()
export class BeauticianServiceService {
  url : String = 'http://localhost:8080/BigStylist';
  constructor(private http: Http) { }
  public addBeautician1(b_name,b_detail,b_mobileno,b_email,b_charges){
    debugger;
    var body = {
      b_name: b_name,
      b_detail: b_detail,
      b_mobileno: b_mobileno,
      b_email:b_email,
      b_charges:b_charges
    };
    var header = new Headers({'Content-Type':'application/json'});
    var requestOption = new RequestOptions({headers: header});
    return this.http.post(this.url + '/addbeautician',body,requestOption);
  }
}

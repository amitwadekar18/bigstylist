import { TestBed, inject } from '@angular/core/testing';

import { BeauticianServiceService } from './beautician-service.service';

describe('BeauticianServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BeauticianServiceService]
    });
  });

  it('should be created', inject([BeauticianServiceService], (service: BeauticianServiceService) => {
    expect(service).toBeTruthy();
  }));
});

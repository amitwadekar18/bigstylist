import { Component, OnInit } from '@angular/core';
import { BeauticianServiceService  } from '../beautician-service.service';
@Component({
  selector: 'app-beautician',
  templateUrl: './beautician.component.html',
  styleUrls: ['./beautician.component.css']
})
export class BeauticianComponent implements OnInit {
b_name:String;
b_detail:String;
b_mobileno:number;
b_email:String;
b_charges:number;
  constructor(private service:BeauticianServiceService) { }
  ngOnInit() {
  }

  public addBeautician(){
    debugger;
    this.service.addBeautician1(this.b_name,this.b_detail,this.b_mobileno,this.b_email,this.b_charges)
    .subscribe( response =>{
      var result = response.json();
      console.log(result);
      if(result.status == 'true'){
        alert("Registered");
      }
      else{
        alert("something wrong");
      }
    });
  }
  public clear(){
  this.b_name="";
  this.b_detail="";
  this.b_email="";
  this.b_mobileno=null;
  this.b_charges=null;
  }
  }
 


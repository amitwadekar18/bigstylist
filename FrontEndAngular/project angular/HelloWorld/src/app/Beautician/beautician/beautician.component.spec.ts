import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BeauticianComponent } from './beautician.component';

describe('BeauticianComponent', () => {
  let component: BeauticianComponent;
  let fixture: ComponentFixture<BeauticianComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BeauticianComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BeauticianComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

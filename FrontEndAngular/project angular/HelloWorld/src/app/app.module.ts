import { loginService } from './User/loginService.component';
import { BeautyServices } from './AllService/beautyservices.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from "@angular/router";
import { ContactUsServiceService } from "./ContactUs/contactusservice.service"

import { AppComponent } from './app.component';
import { LoginComponent } from './User/login/login.component';
import { HttpModule } from '@angular/http';
import { FormsModule } from "@angular/forms";
import { RegisterComponent } from './User/register/register.component';
import { AllservicesComponent } from './AllService/allservices/allservices.component';
import { ContactUsComponent } from './ContactUs/contactus/contactus.component';
import { GallaryComponent } from './Gallary/gallary/gallary.component';
import { HomeComponent } from './Home/home/home.component';
import { StorageServiceModule } from 'angular-webstorage-service';
import { CartComponent } from './Cart/cart/cart.component';
import { AboutUsComponent } from './About Us/about-us/about-us.component';
import { AddServicesComponent } from './AllService/add-services/add-services.component';
import { BeauticianComponent } from './Beautician/beautician/beautician.component';
import { BeauticianServiceService } from './Beautician/beautician-service.service';
import { CartServiceService } from './Cart/cart-service.service';
import { PaymentComponent } from './Cart/payment/payment.component';
import { FinalTransactionComponent } from './Cart/final-transaction/final-transaction.component'

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    AllservicesComponent,
    ContactUsComponent,
    GallaryComponent,
    HomeComponent,
    CartComponent,
    AboutUsComponent,
    AddServicesComponent,
    BeauticianComponent,
    PaymentComponent,
    FinalTransactionComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    RouterModule.forRoot([
      {path: 'register', component: RegisterComponent },
      {path: 'allservices', component: AllservicesComponent },
      {path: 'contactus', component: ContactUsComponent },
      {path: 'gallary', component: GallaryComponent },
      {path: 'home', component: HomeComponent },
      {path: 'cart', component: CartComponent },
      {path: 'aboutus', component: AboutUsComponent },
      {path: 'login', component: LoginComponent },  
      {path: 'addservices', component: AddServicesComponent },   
      {path: 'addbeautician', component: BeauticianComponent },  
      {path: 'payment', component: PaymentComponent },     
      {path: 'finaltransaction', component: FinalTransactionComponent },     
      {path: '**', component: HomeComponent }
    ]),
    StorageServiceModule
  ],
  providers: [loginService,BeautyServices,ContactUsServiceService,BeauticianServiceService,CartServiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }

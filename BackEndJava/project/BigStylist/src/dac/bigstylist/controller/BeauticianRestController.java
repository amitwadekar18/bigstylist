package dac.bigstylist.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

import dac.bigstylist.entities.Beautician;
import dac.bigstylist.entities.Customer;
import dac.bigstylist.entities.Services;
import dac.bigstylist.models.ResponseModel;
import dac.bigstylist.services.BeauticianService;


@RestController
public class BeauticianRestController {
	@Autowired
    private BeauticianService beauticianService;
	
	private ResponseModel respModel;

	ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
	
	@CrossOrigin
	@GetMapping("/allbeautician")
	public String getServices() throws JsonProcessingException 
	{
		System.out.println("in controller");
     try
	{
		List<Beautician>list = beauticianService.getBeautician();
        return ow.writeValueAsString(new ResponseModel("true",list));
    }catch(Exception ex)
	{
  	   ex.printStackTrace();
	}
	  return ow.writeValueAsString(new ResponseModel("false",null));

}
	
	
	
	
	@CrossOrigin
	@PostMapping(value="/addbeautician",headers="Accept=application/json")
	public String addBook(@RequestBody Beautician b) throws JsonProcessingException {
		try {
			beauticianService.insert(b);
			respModel=new ResponseModel("true",null);
			return ow.writeValueAsString(respModel);
		}catch (Exception e) {
			e.printStackTrace();
		}
		return ow.writeValueAsString(new ResponseModel("false",null));
	}
	
	
}

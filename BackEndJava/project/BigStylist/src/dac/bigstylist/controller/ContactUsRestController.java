package dac.bigstylist.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

import dac.bigstylist.entities.ContactUs;
import dac.bigstylist.entities.Customer;
import dac.bigstylist.models.ResponseModel;
import dac.bigstylist.services.ContactUsService;
@RestController
public class ContactUsRestController {
  @Autowired
  private ContactUsService contactusservice;
  private ResponseModel respModel;
  
  ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
    
    @CrossOrigin
	@PostMapping(value="/contactus",headers="Accept=application/json")
	public String addComplaint(@RequestBody ContactUs c) throws JsonProcessingException {
		try {
			contactusservice.addComplaint(c);
			respModel=new ResponseModel("true",null);
			return ow.writeValueAsString(respModel);
		}catch (Exception e) {
			e.printStackTrace();
		}
		return ow.writeValueAsString(new ResponseModel("false",null));
	}
}

package dac.bigstylist.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

import dac.bigstylist.entities.Customer;
import dac.bigstylist.entities.Services;
import dac.bigstylist.models.ResponseModel;
import dac.bigstylist.services.CustomerService;
import dac.bigstylist.services.ServicesService;
@RestController
public class ServicesRestController {
	@Autowired
    private ServicesService servicesService;
	private ResponseModel respModel;
	
	ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();

	@CrossOrigin
	@GetMapping("/allservices")
	public String getServices() throws JsonProcessingException 
	{
		System.out.println("in controller");
     try
	{
		List<Services> list = servicesService.getServices();
        return ow.writeValueAsString(new ResponseModel("true",list));
    }catch(Exception ex)
	{
  	   ex.printStackTrace();
	}
	  return ow.writeValueAsString(new ResponseModel("false",null));

		
	}
	
	
	@CrossOrigin
	@PostMapping(value="/addservices",headers="Accept=application/json")
	public String addServices(@RequestBody Services s) throws JsonProcessingException {
		try {
			servicesService.addServices(s);;
			respModel=new ResponseModel("true",null);
			return ow.writeValueAsString(respModel);
		}catch (Exception e) {
			e.printStackTrace();
		}
		return ow.writeValueAsString(new ResponseModel("false",null));
	}


	
}

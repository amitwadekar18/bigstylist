package dac.bigstylist.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

import dac.bigstylist.entities.Customer;
import dac.bigstylist.entities.Services;
import dac.bigstylist.models.Login;
import dac.bigstylist.models.ResponseModel;
import dac.bigstylist.services.*;



@RestController
public class CustomerRestController {
	@Autowired
    private CustomerService customerService;
	
	private ResponseModel respModel;
	
	ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
	
	@CrossOrigin
	@PostMapping(value="/register",headers="Accept=application/json")
	public String addBook(@RequestBody Customer c) throws JsonProcessingException {
		try {
			customerService.insert(c);
			respModel=new ResponseModel("true",null);
			return ow.writeValueAsString(respModel);
		}catch (Exception e) {
			e.printStackTrace();
		}
		return ow.writeValueAsString(new ResponseModel("false",null));
	}

	
	@CrossOrigin
	@PostMapping(value="/login",headers="Accept=application/json")
	public String doLoginMethod(@RequestBody Login cred) throws JsonProcessingException{
		System.out.println(cred.getC_email());
		System.out.println(cred.getC_password());
        try
        {
		  Customer cust=customerService.validate(cred);
		  respModel=new ResponseModel("true",cust);
		  return ow.writeValueAsString(respModel);
		
        }catch (Exception e) {
			e.printStackTrace();
	    }
        return ow.writeValueAsString(new ResponseModel("false",null));
	}

	
}
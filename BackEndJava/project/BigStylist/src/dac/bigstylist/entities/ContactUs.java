package dac.bigstylist.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Contact_Us_tb")
public class ContactUs implements Serializable {
private  int cu_id;
private String cu_name;
private String cu_email;
private String cu_mobileno;
private String cu_message;
public ContactUs() {
this(0,"","","","");
}
public ContactUs(int cu_id, String cu_name, String cu_email, String cu_mobileno, String cu_message) {
	this.cu_id = cu_id;
	this.cu_name = cu_name;
	this.cu_email = cu_email;
	this.cu_mobileno = cu_mobileno;
	this.cu_message = cu_message;
}
@Id
@Column
public int getCu_id() {
	return cu_id;
}
public void setCu_id(int cu_id) {
	this.cu_id = cu_id;
}

@Column
public String getCu_name() {
	return cu_name;
}
public void setCu_name(String cu_name) {
	this.cu_name = cu_name;
}

@Column
public String getCu_email() {
	return cu_email;
}
public void setCu_email(String cu_email) {
	this.cu_email = cu_email;
}

@Column
public String getCu_mobileno() {
	return cu_mobileno;
}
public void setCu_mobileno(String cu_mobileno) {
	this.cu_mobileno = cu_mobileno;
}

@Column
public String getCu_message() {
	return cu_message;
}
public void setCu_message(String cu_message) {
	this.cu_message = cu_message;
}
@Override
public String toString() {
	return "ContactUs [cu_id=" + cu_id + ", cu_name=" + cu_name + ", cu_email=" + cu_email + ", cu_mobileno="
			+ cu_mobileno + ", cu_message=" + cu_message + "]";
}


}

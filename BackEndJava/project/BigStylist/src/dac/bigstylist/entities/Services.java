package dac.bigstylist.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Service_tb")
public class Services implements Serializable {
	private int s_id;
	private String s_type;
	private String s_name;
	private int s_duration;
	private int s_price;
	public Services() {
	 this(0,"","",0,0);
	}
	public Services(int s_id, String s_type, String s_name, int s_duration, int s_price) {
		this.s_id = s_id;
		this.s_type = s_type;
		this.s_name = s_name;
		this.s_duration = s_duration;
		this.s_price = s_price;
	}
	@Id
	@Column
	public int getS_id() {
		return s_id;
	}
	public void setS_id(int s_id) {
		this.s_id = s_id;
	}
	@Column
	public String getS_type() {
		return s_type;
	}
	public void setS_type(String s_type) {
		this.s_type = s_type;
	}
	@Column
	public String getS_name() {
		return s_name;
	}
	public void setS_name(String s_name) {
		this.s_name = s_name;
	}

	@Column
	public int getS_price() {
		return s_price;
	}
	public void setS_price(int s_price) {
		this.s_price = s_price;
	}
	@Column
	public int getS_duration() {
		return s_duration;
	}
	public void setS_duration(int s_duration) {
		this.s_duration = s_duration;
	}
	@Override
	public String toString() {
		return "Services [s_id=" + s_id + ", s_type=" + s_type + ", s_name=" + s_name + ", s_duration=" + s_duration
				+ ", s_price=" + s_price + "]";
	}

	
}

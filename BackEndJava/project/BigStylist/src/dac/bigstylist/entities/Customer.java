package dac.bigstylist.entities;
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name="Customer_tb")
public class Customer implements Serializable {
private int c_id;
private String c_email;
private String c_name;
private String c_password;
private String c_mobileno;
private String c_address;
private String c_city;
private int c_pincode;
private String role;

public Customer(){
	this(0,"","","","","","",0);
}

public Customer(int c_id, String c_email, String c_name, String c_password, String c_mobileno, String c_address,
		String c_city, int c_pincode){
	this.c_id = c_id;
	this.c_email = c_email;
	this.c_name = c_name;
	this.c_password = c_password;
	this.c_mobileno = c_mobileno;
	this.c_address = c_address;
	this.c_city = c_city;
	this.c_pincode = c_pincode;
	this.role = "customer";
}
/*@GenericGenerator(name="gen",strategy="native")
@GeneratedValue(generator="gen")*/
@Id
@Column(name="c_id")
public int getC_id() {
	return c_id;
}

public void setC_id(int c_id) {
	this.c_id = c_id;
}

@Column
public String getC_email() {
	return c_email;
}

public void setC_email(String c_email) {
	this.c_email = c_email;
}

@Column
public String getC_name() {
	return c_name;
}

public void setC_name(String c_name) {
	this.c_name = c_name;
}

@Column
public String getC_password() {
	return c_password;
}

public void setC_password(String c_password) {
	this.c_password = c_password;
}

@Column
public String getC_mobileno() {
	return c_mobileno;
}

public void setC_mobileno(String c_mobileno) {
	this.c_mobileno = c_mobileno;
}

@Column
public String getC_address() {
	return c_address;
}

public void setC_address(String c_address) {
	this.c_address = c_address;
}

@Column
public String getC_city() {
	return c_city;
}

public void setC_city(String c_city) {
	this.c_city = c_city;
}

@Column
public int getC_pincode() {
	return c_pincode;
}

public void setC_pincode(int c_pincode) {
	this.c_pincode = c_pincode;
}

@Column
public String getRole() {
	return role;
}

public void setRole(String role) {
	this.role = role;
}

@Override
public String toString() {
	return "Customer [c_id=" + c_id + ", c_email=" + c_email + ", c_name=" + c_name + ", c_password=" + c_password
			+ ", c_mobileno=" + c_mobileno + ", c_address=" + c_address + ", c_city=" + c_city + ", c_pincode="
			+ c_pincode + ", role=" + role + "]";
} 

}

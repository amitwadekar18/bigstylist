package dac.bigstylist.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Beautician_tb")
public class Beautician implements Serializable {
  private int b_id;
  private String b_name;
  private String b_details;
  private String b_mobileno;
  private String b_email;
  private int b_charges;

  public Beautician() {
	this(0,"","","","",0);
}

public Beautician(int b_id, String b_name, String b_details, String b_mobileno, String b_email, int b_charges) {
	
	this.b_id = b_id;
	this.b_name = b_name;
	this.b_details = b_details;
	this.b_mobileno = b_mobileno;
	this.b_email = b_email;
	this.b_charges = b_charges;
}

@Id
@Column
public int getB_id() {
	return b_id;
}

public void setB_id(int b_id) {
	this.b_id = b_id;
}

@Column
public String getB_name() {
	return b_name;
}

public void setB_name(String b_name) {
	this.b_name = b_name;
}

@Column
public String getB_details() {
	return b_details;
}

public void setB_details(String b_details) {
	this.b_details = b_details;
}

@Column
public String getB_mobileno() {
	return b_mobileno;
}

public void setB_mobileno(String b_mobileno) {
	this.b_mobileno = b_mobileno;
}

@Column
public String getB_email() {
	return b_email;
}

public void setB_email(String b_email) {
	this.b_email = b_email;
}

@Column
public int getB_charges() {
	return b_charges;
}

public void setB_charges(int b_charges) {
	this.b_charges = b_charges;
}

@Override
public String toString() {
	return "Beautician [b_id=" + b_id + ", b_name=" + b_name + ", b_details=" + b_details + ", b_mobileno=" + b_mobileno
			+ ", b_email=" + b_email + ", b_charges=" + b_charges + "]";
}
  


  
  
}

package dac.bigstylist.services;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import dac.bigstylist.dao.BeauticianDao;
import dac.bigstylist.dao.CustomerDao;
import dac.bigstylist.entities.Beautician;
import dac.bigstylist.entities.Services;

@Service
public class BeauticianServiceImpl implements BeauticianService {
    
	@Autowired
	private BeauticianDao dao;
	
	@Transactional
	@Override
	public List<Beautician>getBeautician() {
	  System.out.println("inside service");
	  List<Beautician>list=dao.getBeautician();
	  return list;
	}

	@Transactional
	@Override
	public void insert(Beautician b) {
		dao.add(b);
		
	}

}

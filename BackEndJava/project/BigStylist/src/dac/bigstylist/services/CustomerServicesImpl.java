package dac.bigstylist.services;
 import dac.bigstylist.services.*;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import dac.bigstylist.dao.*;
import dac.bigstylist.entities.Customer;
import dac.bigstylist.entities.Services;
import dac.bigstylist.models.Login;
@Service
public class CustomerServicesImpl implements CustomerService {

	@Autowired
	private CustomerDao dao;
   
	@Transactional
	@Override
	public void insert(Customer c) {
	dao.add(c);
	}

	@Transactional
	@Override
	public Customer validate(Login cred) {
		System.out.println("service="+cred.getC_email());
		Customer c=dao.getCustomer(cred.getC_email());
		if(c!=null && c.getC_password().equals(cred.getC_password()))
			return c;
		return null;
	}
	
	
	


}

package dac.bigstylist.services;

import java.util.List;

import dac.bigstylist.entities.Services;

public interface ServicesService {
	 List<Services>getServices();
	 void addServices(Services s);
}

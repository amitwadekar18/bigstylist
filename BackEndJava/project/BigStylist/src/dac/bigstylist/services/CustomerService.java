package dac.bigstylist.services;
import java.util.List;

import dac.bigstylist.entities.*;
import dac.bigstylist.models.Login;
public interface CustomerService {
	void insert(Customer c);
    Customer validate(Login cred);
   
}

package dac.bigstylist.services;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import dac.bigstylist.dao.CustomerDao;
import dac.bigstylist.dao.ServicesDao;
import dac.bigstylist.entities.Services;

@Service
public class ServicesServiceImpl implements ServicesService {

	@Autowired
	private ServicesDao dao;
	
	@Transactional
	@Override
	public List<Services> getServices() {
      System.out.println("inside service");
       List<Services>list=dao.getServices();
       for (Services services : list) {
    	   System.out.println(services);
		}
		return list;
	}

	@Transactional
	@Override
	public void addServices(Services s) {
      dao.AddService(s);
	}
}

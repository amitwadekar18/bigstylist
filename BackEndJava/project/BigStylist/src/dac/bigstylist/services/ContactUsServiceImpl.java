package dac.bigstylist.services;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import dac.bigstylist.dao.ContactUsDao;
import dac.bigstylist.dao.CustomerDao;
import dac.bigstylist.entities.ContactUs;

@Service
public class ContactUsServiceImpl implements ContactUsService {
    @Autowired
	private ContactUsDao dao;
 
    @Transactional
	@Override
	public void addComplaint(ContactUs us){
	dao.addComplaint(us);
	}

}

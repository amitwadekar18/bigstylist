package dac.bigstylist.dao;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import dac.bigstylist.dao.*;
import dac.bigstylist.entities.ContactUs;

@Repository
public class ContactUsDaoImpl implements ContactUsDao{

	@Autowired
	private SessionFactory factory;
	
	@Override
	public void addComplaint(ContactUs us) {
	 System.out.println("inside the dao="+us.getCu_name());
	 Session session = factory.getCurrentSession();
	 session.persist(us);
		
	}

}

package dac.bigstylist.dao;

import java.util.List;

import dac.bigstylist.entities.Services;

public interface ServicesDao {
	List<Services> getServices();
	void AddService(Services s);
}

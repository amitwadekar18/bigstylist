package dac.bigstylist.dao;
import java.util.List;
import dac.bigstylist.entities.*;

public interface BeauticianDao {
	void add(Beautician b);
	List<Beautician> getBeautician();
}

package dac.bigstylist.dao;
import java.io.Serializable;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import dac.bigstylist.entities.Services;

@Repository
public class ServicesDaoImpl implements ServicesDao {
   @Autowired
   private SessionFactory factory;
   
   @Override
	public List<Services> getServices() {
		 Session session = factory.getCurrentSession();
	     Query q = session.createQuery("from Services");
	     return (List<Services>)q.getResultList();
		
	}

   @Override
   public void AddService(Services s) {
	Session session = factory.getCurrentSession();
    session.persist(s);
}

}

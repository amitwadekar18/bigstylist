package dac.bigstylist.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import dac.bigstylist.entities.Beautician;
import dac.bigstylist.entities.Services;

@Repository
public class BeauticianDaoImpl implements BeauticianDao{
    @Autowired
	private SessionFactory factory;
	
	@Override
	public List<Beautician> getBeautician() {
		 Session session = factory.getCurrentSession();
	     Query q = session.createQuery("from Beautician");
	     return (List<Beautician>)q.getResultList();
		
	}

	@Override
	public void add(Beautician b) {
		Session session = factory.getCurrentSession();
	    session.persist(b);
		
	}

}

package dac.bigstylist.dao;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import dac.bigstylist.dao.*;
import dac.bigstylist.entities.Customer;
import dac.bigstylist.entities.Services;
@Repository
public class CustomerDaoImpl implements CustomerDao {
    @Autowired
	private SessionFactory factory;
	@Override
	public void add(Customer c) {
		Session session = factory.getCurrentSession();
	    session.persist(c);
	}
	@Override
   public Customer getCustomer(String email)
   {
		System.out.println("dao="+email);
	    Session session = factory.getCurrentSession();
        Query q = session.createQuery("from Customer c where c.c_email=:p_email");
        q.setParameter("p_email", email);
        
        Customer customer=(Customer)q.getSingleResult();
        System.out.println(customer.getC_name());
        
        if(customer!=null)
        {
          System.out.println(customer.getC_name());
           return customer;
        }
        else
        	return null;
        }
	
}

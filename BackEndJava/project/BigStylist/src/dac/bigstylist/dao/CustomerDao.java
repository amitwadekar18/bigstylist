package dac.bigstylist.dao;
import java.util.List;

import dac.bigstylist.entities.*;
public interface CustomerDao {
	void add(Customer c);
	Customer getCustomer(String email);
	

}

package dac.bigstylist.models;

public class Login {
	private String c_email;
	private String c_password;
	public Login() {
		
	}
	public Login(String c_email, String c_password) {
        this.c_email = c_email;
		this.c_password = c_password;
	}
	public String getC_email() {
		return c_email;
	}
	public void setC_email(String c_email) {
		this.c_email = c_email;
	}
	public String getC_password() {
		return c_password;
	}
	public void setC_password(String c_password) {
		this.c_password = c_password;
	}
	@Override
	public String toString() {
		return "Login [c_email=" + c_email + ", c_password=" + c_password + "]";
	}
	
	
	

}
